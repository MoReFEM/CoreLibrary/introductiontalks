Here are some talks which goal is to give hints of the way MoReFEM works. They are few years old now but remains mostly up-to-date:

- Lexicon is a slideshow prepared to explain the key concepts of the library in up-to-three slides each.

- Sebastien's talk (_Overview_Sebastien_) is more of an overview, and deals with generic explanations (e.g. about mpi parallelism, C++ principles and so forth). You also get there useful command lines to get started (e.g. how to clone the sources or install the third party libraries).

- Gautier's talk (_ModelInstances_Gautier_) is much more practical: it explains how to set up a ModelInstance, using as example the case of hyperelasticity with midpoint scheme. It also provides enlightening slides about the vector numbering, which are much more detailed than those in Sebastien's talk.

- Gautier also prepared a more recent overview (_Overview_Gautier_) for M3DISIM team evaluation.


Each of these talks are present in 4 formats:

- The original Keynote one; which is the reference. Use it if you're using a Mac!
- An html format. The way to go if you want to see original presentation without a Mac at hand (especially if there are animations involved).
- A pdf with a huge number of slides to mimic the animations.
- A Powerpoint conversion. Not that bad, but some renderings are just awful.
